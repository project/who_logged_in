CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------
The Who Logged In module creates a block that lists users who logged into the
site to a certain time period. You can configure the module to any time you like
but by default it is set to 24 hours.


REQUIREMENTS
------------
 * This module does not have any requirements.


INSTALLATION
------------
 * Install as you would normally install a contributed drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.
 * Set up the block in the correct region in the blocks
   configration: admin/structure/block

CONFIGURATION
-------------
You can set the amount of past hours to display the users at
/admin/config/people/who-logged-in


MAINTAINERS
-----------
Current maintainer:
 * Albert Jankowski (albertski) - https://www.drupal.org/u/albertski
